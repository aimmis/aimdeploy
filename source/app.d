import std.stdio;

import aim.deploy.commands;
import jaster.cli.core;

void main(string[] args)
{
	import scriptlike;

	scriptlikeEcho = true;
	runCliCommands!(aim.deploy.commands)(args);
}
