module aim.deploy.util;

private
{
    import scriptlike;
    import scriptlike : scriptExecuteShell = executeShell;
}

public
{
    import std.format : format;
}

static class Util
{
    // Common
    public static
    {
        void enforceCommandExists(string command)
        {
            import std.exception : enforce;

            enforce(Util.commandExists(command), "The command '"~command~"' is required, but cannot be found.");
        }

        auto executeShell(string command)
        {
            import std.stdio : writeln;

            if(scriptlikeEcho)
                writeln("executeShell: ", command);

            auto data = scriptExecuteShell(command);
            writeln(data.output);
            
            return data;
        }

        auto executeShellEnforceZero(string command)
        {
            import std.exception : enforce;

            auto result = Util.executeShell(command);
            enforce(result.status == 0, "The command '"~command~"' did not return status code 0.\n\n"~result.output);
            return result;
        }
    }

    // Platform specific implementations
    public static
    {
        version(linux)
        {
            bool commandExists(string command)
            {
                import std.ascii     : isWhite;
                import std.algorithm : all;

                auto output = executeShell("which "~command).output;
                return !output.all!isWhite;
            }

            Path getGlobalConfigDir(string subfolder = null)
            {
                auto path = Path("/etc/aimdeploy/") ~ subfolder;
                if(!path.exists)
                    mkdirRecurse(path.dirName);

                return path;
            }
        }
        else version(Windows)
        {
            bool commandExists(string command)
            {
                return false;
            }

            Path getGlobalConfigDir(string subfolder = null)
            {
                auto path = Path("C:/ProgramData/aimdeploy/") ~ subfolder;
                if(!path.exists)
                    mkdirRecurse(path.dirName);

                return path;
            }
        }
        else static assert(false);
    }
}