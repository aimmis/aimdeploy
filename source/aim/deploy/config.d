module aim.deploy.config;

private
{
    import std.typecons : Nullable, Flag;
    import jaster.serialise;
    import aim.deploy.util;
    import scriptlike.path, scriptlike.file;
}

alias IgnoreIfMissing = Flag!"ignoreMissing";

@Name("var")
struct DeployConfigVar
{
    string name;
    string description;
}

struct DeployConfig
{
    string name;
    string domain;
    Nullable!string outputFileName;
    ushort port;

    @Setting(Serialiser.Settings.ArrayAsObject)
    DeployConfigVar[] requiredVars;

    static DeployConfig fromFile(Path file)
    {
        return file.parseConfig!DeployConfig(IgnoreIfMissing.no);
    }

    bool isValidVarName(string name)
    {
        import std.algorithm : any;
        return this.requiredVars.any!(v => v.name == name);
    }

    auto getMissingVars(ProjectConfig projConfig)
    {
        import std.algorithm : filter, any;
        return this.requiredVars.filter!(v => !projConfig.envVars.any!(v2 => v2.name == v.name));
    }
}

struct ProjectEnvVar
{
    string name;
    string value;
}

enum ProjectOriginType
{
    None,
    Github
}

struct ProjectReadAccess
{
    ProjectOriginType type;
    string username;
    string token;
    string previousTag;
    string repoGroup;
    string repoName;
}

// TODO: A way to read in old versions, then convert them into the latest version.
struct ProjectConfig
{
    static const CURRENT_VERSION = 1;

    @Name("version")
    int version_ = CURRENT_VERSION;

    @Setting(Serialiser.Settings.ArrayAsObject)
    ProjectEnvVar[] envVars;

    Nullable!ProjectReadAccess readAccess;

    ref ProjectEnvVar getVarByRef(string name)
    {
        foreach(ref var; this.envVars)
        {
            if(var.name == name)
                return var;
        }

        this.envVars ~= ProjectEnvVar(name, "");
        return this.envVars[$-1];

        assert(false);
    }

    static ProjectConfig fromFileReadOnly(Path file)
    {
        return file.parseConfig!ProjectConfig;
    }

    static void fromFileReadWrite(Path file, void delegate(ref ProjectConfig) func)
    {
        auto obj = ProjectConfig.fromFileReadOnly(file);
        func(obj);

        auto archive = new ArchiveSDL();
        Serialiser.serialise(obj, archive.root);
        archive.saveToFile(file.raw);
    }
}

struct GlobalConfig
{
    string[] projects;

    static GlobalConfig fromFileReadOnly()
    {
        return Util.getGlobalConfigDir("config.sdl").parseConfig!GlobalConfig;
    }

    static void fromFileReadWrite(void delegate(ref GlobalConfig) func)
    {
        auto obj = GlobalConfig.fromFileReadOnly();
        func(obj);

        auto archive = new ArchiveSDL();
        Serialiser.serialise(obj, archive.root);
        archive.saveToFile(Util.getGlobalConfigDir("config.sdl").raw);
    }
}

private T parseConfig(T)(Path file, IgnoreIfMissing ignore = IgnoreIfMissing.yes)
{
    if(!file.exists && ignore)
    {
        mkdirRecurse(file.dirName);
        return T();
    }

    auto archive = new ArchiveSDL();
    archive.loadFromFile(file.raw);

    UsedObjectsT used;
    auto obj = Serialiser.deserialise!T(archive.root, used);
    used.enforceAllChildrenUsed();

    return obj;
}