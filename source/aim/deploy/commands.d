module aim.deploy.commands;

private
{
    import std.exception;
    import jaster.cli.udas;
    import scriptlike;
    import aim.deploy.util, aim.deploy.config;

    const PROJECT_CONFIG_FILE = ".aimdeploy/config.sdl";
    const DEPLOY_CONFIG_FILE  = "aimdeploy.sdl";
    const NGINX_SERVER_BLOCK  = import("nginx_server_block.txt"); // Format string. 0 = domain, 1 = port.
    const SYSTEMD_SERVICE     = import("systemd_service.service"); // Format string. 0 = name, 1 = working directory, 2 = dll to execute, 3 = port, 4 = env vars.
}

@Command
@CommandName("publish")
@CommandDescription("Compiles the project, and sets it up as a systemd service. (among other stuff)")
struct PublishCommand
{
    DeployConfig _deploy;
    ProjectConfig _project;

    void onExecute()
    {
        this._deploy = DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));
        this._project = ProjectConfig.fromFileReadOnly(Path(PROJECT_CONFIG_FILE));
        this.onExecuteImpl();
    }

    version(linux)
    {
        void onExecuteImpl()
        {
            scriptlikeEcho = true; // TEMP until bool arguments are supported.
            this.dependencyCheck();
            this.setupNginxAndLetsEncrypt();
            this.publish();
        }

        void dependencyCheck()
        {
            Util.enforceCommandExists("nginx");
            Util.enforceCommandExists("git");
            Util.enforceCommandExists("dotnet");
            Util.enforceCommandExists("certbot");
            Util.enforceCommandExists("ln");
        }

        void setupNginxAndLetsEncrypt()
        {
            import std.array : replace;

            writeln("[NginxAndLet'sEncrypt]");

            const nginxFile = buildNormalizedPathFixed("/etc/nginx/sites-available/", this._deploy.name);
            if(nginxFile.existsAsFile)
            {
                writeln("nginx is configured, skipping...");
                return;
            }

            // Create the nginx server block file.
            writeln("Creating nginx server block file...");
            writeFile(Path(nginxFile), format(NGINX_SERVER_BLOCK, this._deploy.domain, this._deploy.port));

            // Make nginx aware of it.
            writeln("Enabling nginx server block...");
            const linkLocation = nginxFile.replace("sites-available", "sites-enabled");
            Util.executeShell(format("ln -s %s %s", nginxFile, linkLocation));

            // Let's encrypt
            writeln("Enabling Certbot for the new domain...");
            Util.executeShell(format("certbot --nginx -d %s --non-interactive -m bradley.chatha@gmail.com", this._deploy.domain));

            writeln("Restarting nginx...");
            Util.executeShell("service nginx restart");
        }

        void publish()
        {
            import std.array     : replace;
            import std.algorithm : map, reduce;
            import std.range     : chain;
            import std.exception : enforce;

            writeln("[Publish]");
            
            // Make sure all env vars have been specified.
            writeln("Checking for missing vars...");
            auto missing = this._deploy.getMissingVars(this._project);
            if(!missing.empty)
            {
                char[] message;
                message ~= "The following Environment Vars must be specified:\n";
                foreach(var; missing)
                    message ~= format("\t%s - %s\n", var.name, var.description);

                message ~= "\nYou can use the `aimdeploy env set` command to set the values.\n";
                message ~= "Alternatively you can modify the file at %s".format(getcwd() ~ PROJECT_CONFIG_FILE);

                throw new Exception(message.idup);
            }

            // Publish with dotnet.
            writeln("Stopping service if it's already running...");
            Util.executeShell("systemctl stop "~this._deploy.name.setExt(".service"));

            writeln("Publishing with dotnet...");
            auto result = Util.executeShell("dotnet publish -o ../.aimdeploy/build/ -c Release");
            enforce(result.status == 0, "`dotnet publish` returned a non-0 exit code.");

            // Create the service file.
            auto premadeEnvVars = 
            [
                ProjectEnvVar("ASPNETCORE_HTTPS_PORT", "443")
            ];

            auto outputDir = buildNormalizedPathFixed(getcwd().raw, ".aimdeploy/build/");
            writeln("Creating service...");
            writeFile(
                Path("/etc/systemd/system/" ~ this._deploy.name.setExt(".service")),
                format(SYSTEMD_SERVICE,
                    this._deploy.name,
                    outputDir,
                    format("\"%s\"", buildNormalizedPathFixed(outputDir, this._deploy.outputFileName.get(this._deploy.name).setExt(".dll"))),
                    this._deploy.port,
                    this._project.envVars
                                 .chain(premadeEnvVars)
                                 .map!(v => format("Environment='%s=%s'", v.name.replace(":", "__"), v.value))
                                 .reduce!((str, str2) => str ~ "\n" ~ str2)
                )
            );
        }
    }
    else version(Windows)
    {
        void onExecuteImpl()
        {
            assert(false, "Not implemented yet, just here so we can compile on Windows.");
        }
    }
    else static assert(false, "Platform not supported.");
}

@Command
@CommandGroup("env")
@CommandName("list-all")
@CommandDescription("Lists all of the environment vars that need to be specified.")
struct EnvListAllCommand
{
    DeployConfig _deploy;

    void onExecute()
    {
        this._deploy = DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));

        char[] message;
        foreach(var; this._deploy.requiredVars)
            message ~= format("\t%s - %s\n", var.name, var.description);

        writeln(message);
    }
}

@Command
@CommandGroup("env")
@CommandName("list-missing")
@CommandDescription("Lists all missing environment vars that still need to be specified.")
struct EnvListMissingCommand
{
    DeployConfig _deploy;

    void onExecute()
    {
        this._deploy = DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));

        char[] message;
        foreach(var; this._deploy.getMissingVars(ProjectConfig.fromFileReadOnly(Path(PROJECT_CONFIG_FILE))))
            message ~= format("\t%s - %s\n", var.name, var.description);

        writeln(message);
    }
}

@Command
@CommandGroup("env")
@CommandName("set")
@CommandDescription("Sets the value for one of the Environment Vars specified in the deploy config.")
struct EnvSetCommand
{
    DeployConfig _deploy;

    @Argument
    @ArgumentRequired
    @ArgumentIndex(0)
    @ArgumentDescription("The name of the environment var.")
    string name;

    @Argument
    @ArgumentRequired
    @ArgumentIndex(1)
    @ArgumentDescription("The value to give the environment var. (Use speech marks if it has spaces)")
    string value;

    void onExecute()
    {
        this._deploy = DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));

        if(!this._deploy.isValidVarName(this.name))
            throw new Exception("The variable '"~this.name~"' does not exist.");

        ProjectConfig.fromFileReadWrite(Path(PROJECT_CONFIG_FILE), (ref project)
        {
            project.getVarByRef(this.name).value = value;
        });
    }
}

@Command
@CommandGroup("monitor")
@CommandName("setup")
@CommandDescription("Sets up the required configuration values for this project to be monitored.")
struct MonitorSetupCommand
{
    // TODO: At the moment, this is forecfully interactive.
    //       At some point, allow args to be passed so it adds the possibility of this becoming automatable.
    void onExecute()
    {
        import std.algorithm;
        import std.regex;

        // Just to make sure it's there and setup properly.
        DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));

        // Get what repo it's being stored on.
        auto shellResult = Util.executeShell("git remote get-url origin");
        auto regexResult = matchFirst(shellResult.output, regex(`https:\/\/.*(github)\.com\/(.*)/(.*)`));
        enforce(!regexResult.empty, "Could not determine git repo type. Supported types are (github)");
        auto type = regexResult[1];

        // Set the config.
        switch(type)
        {
            case "github":
                ProjectConfig.fromFileReadWrite(Path(PROJECT_CONFIG_FILE), (ref ProjectConfig conf)
                {
                    conf.readAccess = ProjectReadAccess(
                        ProjectOriginType.Github,
                        userInput!string("Enter your Github username:"),
                        userInput!string("\nEnter a personal access token so that this repo can be accessed:"),
                        "",
                        regexResult[2],
                        regexResult[3]
                    );
                });
                break;

            default:
                throw new Exception("Unknown git repo type: " ~ type);
        }

        // Add it to the list of projects.
        GlobalConfig.fromFileReadWrite((ref GlobalConfig config)
        {
            if(!config.projects.canFind(getcwd().raw))
                config.projects ~= getcwd().raw;
        });
    }
}

@Command
@CommandGroup("monitor")
@CommandName("start")
@CommandDescription("[Service mode] Begins monitoring all registered projects.")
struct MonitorStartCommand
{
    void onExecute()
    {
        import std.algorithm;
        import std.array;
        import std.range;
        import std.string : stringStrip = strip;
        import std.exception;
        import std.format;
        import core.thread, core.time;

        auto globalConfig = GlobalConfig.fromFileReadOnly();

        while(true)
        {
            foreach(project; globalConfig.projects.filter!(p => p.exists))
            {
                chdir(project);

                try
                {
                    auto projectConfig = ProjectConfig.fromFileReadOnly(Path(PROJECT_CONFIG_FILE));
                    enforce(!projectConfig.readAccess.isNull && projectConfig.readAccess.type != ProjectOriginType.None, 
                            "Please run 'aimdeploy monitor setup' first.");
                    assert(projectConfig.readAccess.type == ProjectOriginType.Github, "TODO");

                    if(!Util.executeShell("git remote get-url origin").output.canFind("@github"))
                    {
                        Util.executeShellEnforceZero(format(
                            "git remote set-url origin \"https://%s:%s@github.com/%s/%s\"",
                            projectConfig.readAccess.username,
                            projectConfig.readAccess.token,
                            projectConfig.readAccess.repoGroup,
                            projectConfig.readAccess.repoName
                        ));
                    }

                    Util.executeShellEnforceZero("git fetch --tags");
                    auto tag = Util.executeShellEnforceZero("git tag").output.stringStrip.splitter("\n").array[$-1];

                    if(tag == projectConfig.readAccess.previousTag)
                        continue;

                    ProjectConfig.fromFileReadWrite(Path(PROJECT_CONFIG_FILE), (ref ProjectConfig conf)
                    {
                        conf.readAccess.previousTag = tag;
                    });

                    Util.executeShellEnforceZero("git checkout master");
                    Util.executeShellEnforceZero("git pull");
                    Util.executeShellEnforceZero("git checkout tags/"~tag);
                    Util.executeShellEnforceZero("aimdeploy publish");

                    // The sleep is so the OS can catch up to the service file being created.
                    Thread.sleep(5.seconds);
                    
                    writeln("Restarting project service...");
                    auto deployConfig = DeployConfig.fromFile(Path(DEPLOY_CONFIG_FILE));
                    Util.executeShellEnforceZero("systemctl start "~deployConfig.name.setExt(".service"));
                }
                catch(Exception ex)
                {
                    std.file.write("AimDeploy.error", ex.msg ~ "\n\n" ~ ex.info.toString());
                    writeln(ex.msg);
                    writeln(ex.info.toString());
                }
            }

            Thread.sleep(30.seconds);
        }
    }
}