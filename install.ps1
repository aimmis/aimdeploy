if ([System.Environment]::OSVersion.Platform -ne [System.PlatformID]::Unix) {
    throw "Only UNIX is supported right now. Certbot, pls come to windows ;(";
}

$CWD = $(Get-Location)
$OutputFile = [System.IO.Path]::Combine($CWD, "aimdeploy")

$ServiceFile = @"
[Unit]
Description=Service to start aimdeploy in monitor mode.
After=network.target
StartLimitIntervalSec=0

[Service]
WorkingDirectory=$CWD
ExecStart="$OutputFile" monitor start
Type=simple
Restart=always
RestartSec=10
KillSignal=SIGINT
User=root

[Install]
WantedBy=multi-user.target
"@

& dub build
& rm "/usr/bin/aimdeploy"
& ln $OutputFile "/usr/bin/aimdeploy"

Out-File -FilePath "/etc/systemd/system/aimdeploy_monitor.service" -InputObject $ServiceFile -Force -Encoding ascii